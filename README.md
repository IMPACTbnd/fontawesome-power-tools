# FontAwesome Power Tools #

![FONTAWESOME_POWER_TOOLS_SS](https://bytebucket.org/IMPACTbnd/fontawesome-power-tools/raw/265091a8c1f046e08b927c0d614dd492b92d068f/fontawesome-powertools-screenshot.jpg)

### Font Awesome is awesome enough, why power tools? ###

* A Chrome extensions that add a power menu to the FontAwesome icons page to speed up workflow.
* Best of both worlds! All the benefits of the Cheatsheet while maintaining the sections and power search function of the Icons page. Faster than ctrl+F and shows related icons!
* <i> tag, SCSS selector, unicode, icon quick copy option. Paste icon right into photoshop!


### How do I get set up? ###

* Clone repo or download zip file and unzip.
* Chrome -> More Tools -> Extensions.
* Up top check the “Developer mode” check box.
* Click "Load Unpacked Extension...".
* Browse to "fontawesome-power-tools" folder.
* Profit!


### Issues/Bugs ###

* **Version 0.1.5**
* You tell me.


### Future updates ###

* loading animation
* png generation
* add to chrome app store
* ?

### Thanks to ###

* [Tooltipster](http://iamceege.github.io/tooltipster/)
