// When enabled overrides the default behavior on the FontAwesome icons pages an adds a tooltip power menu
function copy(el, str) {
	$(el).parent().css("opacity", "1");
    var sandbox = $(el).val(str).select();
    document.execCommand('copy');
}

function powerMenu(command, container) {
	console.log("Container is: "+container);
	if(!container) { container='#icons'; }
	if(command==="start") {
		$("#icons").queue( "steps", function( next ) {
			$(".fa-hover a").click(function(e) {
				//e.preventDefault();
			});
			next();
		}).delay(500, "steps").queue( "steps", function( next ) {
			$(container).find(".fa-hover").each(function(i) {
				//console.log(i);
				$(this).find("i").wrap( "<span class='icon-holder'></span>" );
				var icon = $(this).find(".icon-holder");
				var faclass = $(icon).find("i").attr("class");
				var iconString = $(this).find("a").html();
				var href = $(icon).parent().attr("href");
				var content = $('<div class="tooltip-inside"><h4>'+iconString+' <small class="text-success">quick copy:</small></h4> \
					<div class="btn-group" role="group" style="font-size:12px;"> \
					<a class="btn btn-sm btn-default tooltip-copy-anchor i-tag" href="#"><i class="fa fa-code"></i> i tag</a> \
					<a class="btn btn-sm btn-primary tooltip-copy-anchor scss" href="#"><i class="fa fa-css3"></i> SCSS</a> \
					<a class="btn btn-sm btn-danger tooltip-copy-anchor unicode" href="#"><i class="fa fa-terminal"></i> UTF-16</a> \
					<a class="btn btn-sm btn-success tooltip-copy-anchor icon" href="#"><i class="fa '+faclass+'"></i> Icon</a> \
					<div class="btn-group" role="group"> \
					<button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> \
					PNG <span class="caret"></span> \
					</button> \
					<ul class="dropdown-menu"> \
					<li><a href="#"><span style="display:inline-block; width:60px">16px</span> <i class="fa fa-picture-o fa-fw"></i></a></li> \
					<li><a href="#"><span style="display:inline-block; width:60px">32px</span> <i class="fa fa-picture-o fa-lg fa-fw"></i></a></li> \
					<li><a href="#"><span style="display:inline-block; width:60px">48px</span> <i class="fa fa-picture-o fa-2x fa-fw"></i></a></li> \
					<li><a href="#"><span style="display:inline-block; width:60px">64px</span> <i class="fa fa-picture-o fa-3x fa-fw"></i></a></li> \
					<li><a href="#"><span style="display:inline-block; width:60px">128px</span> <i class="fa fa-picture-o fa-4x fa-fw"></i></a></li> \
					</ul> \
					</div> \
					</div> \
					<label style="opacity:0; position: relative; overflow:hidden; vertical-align: middle;">Copied! <input class="sandbox" type="text" style="position:absolute; bottom:-100px;"></label></div>');

				$(this).tooltipster({
					theme: 'tooltipster-shadow',
					interactive: true,
					autoClose: true,
					position: 'left',
					offsetX: '-25',
					speed: 100,
					onlyOne: true,
				    content: content,
					functionReady: function(origin, tooltip) {
				        $(tooltip).find(".tooltip-copy-anchor").on("click", function(e) {
							e.preventDefault();
							var sandbox = $(this).parents(".tooltip-inside").find(".sandbox");
							var str;

							if($(this).hasClass("i-tag")) {
								str = $(icon).html();
								copy(sandbox, str);
							} 
							else if ($(this).hasClass("scss")) {
								str = faclass.replace("fa ", ".");
								copy(sandbox, str);
							}
							else if($(this).hasClass("unicode")) {
								faclass = faclass.replace("fa ", ".");
								
								var str = window.getComputedStyle($(icon).find(faclass).get(0),':before').content.charCodeAt(1).toString(16);
								console.log("this 1: "+str);
							    copy(sandbox, str);
							}
							else if($(this).hasClass("icon")) {
								faclass = faclass.replace("fa ", ".");
								$(sandbox).css("font-family", "FontAwesome");
								
								var str = window.getComputedStyle($(icon).find(faclass).get(0),':before').content;
								console.log("this 2: "+str);
							    copy(sandbox, str.replace(/"/g, ''));
							}
						});
				    },
				    functionBefore: function(origin, continueTooltip) { 
				    	$(origin).addClass("tooltipped");
				    	continueTooltip();
				    },
				    functionAfter: function(origin) {
				    	$(origin).removeClass("tooltipped");
				    	$(origin).tooltipster('content', content);
				    }
				});
			});
			next();
		}).dequeue( "steps" );
	}
	else {
		$(".fa-hover").tooltipster('destroy');
		//$(".fa-hover a").unbind('click');
	}
}

$("#search-input").on("change", function() {
	var val = $(this).val();
	
	powerMenu('start', '#search-results');
});

chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {      
    if (msg.command) {                          
        powerMenu(msg.command, null);
        sendResponse({status: "Command Sent: "+msg.command});       
    }
    return true;
});