
chrome.webNavigation.onCompleted.addListener(function(details) {
    var currentState = localStorage.currentState || "start";
    var tabId = details.tabId;

    if(currentState==="start"){
	    localStorage.currentState="start";

    	chrome.tabs.query({currentWindow: true}, function(tabs) {
		  chrome.tabs.sendMessage(tabId, {command: "start"}, function(response) {

		  });
		});
	}
	else {
		chrome.browserAction.setIcon({path: "disabled_icon_48.png"});
	}
}, {url: [{hostSuffix: "fortawesome.github.io", pathContains: "Font-Awesome/icons"}]});


// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function(tab) {
    // No tabs or host permissions needed!
    var isAwesome = tab.url.indexOf("://fortawesome.github.io/Font-Awesome/icons/");
    var tabId = tab.index;
    
    if(isAwesome>-1) {
	    var currentState = localStorage.currentState || "start";
	    if(currentState==="start"){
		    localStorage.currentState="stop";

		    chrome.tabs.query({currentWindow: true}, function(tabs) {
			  chrome.tabs.sendMessage(tabs[tabId].id, {command: "stop"}, function(response) {
			    
			  });
			});
		    chrome.browserAction.setIcon({path: "disabled_icon_48.png"});
		}
		if(currentState==="stop"){
		    localStorage.currentState="start";

		    chrome.tabs.query({currentWindow: true}, function(tabs) {
			  chrome.tabs.sendMessage(tabs[tabId].id, {command: "start"}, function(response) {
			    
			  });
			});
			chrome.browserAction.setIcon({path: "icon_48.png"});
		}
	}
});

chrome.runtime.onInstalled.addListener(function(details) {
	chrome.tabs.query({currentWindow: true}, function(tabs) {
		for (var i = 0; i < tabs.length; i++) {
		   var isAwesome = tabs[i].url.indexOf("://fortawesome.github.io/Font-Awesome/icons/");
		   if(isAwesome>-1) {
		   		//console.log("font awesome icon page is open, running function");
		   		localStorage.currentState="start";
		   		chrome.tabs.reload(tabs[i].id);
		   }
		}
	});
});
